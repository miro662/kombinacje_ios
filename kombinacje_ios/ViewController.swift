//
//  ViewController.swift
//  kombinacje_ios
//
//  Created by Mirosław Błażej on 22.06.2017.
//  Copyright © 2017 Mirosław Błażej. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var kStepper: UIStepper!
    @IBOutlet weak var nStepper: UIStepper!
    
    @IBOutlet weak var kLabel: UILabel!
    @IBOutlet weak var nLabel: UILabel!
    
    @IBOutlet weak var noOfCombinationsLabel: UILabel!
    
    //MARK: Values
    
    var k:Int = 1 {
        didSet {
            kLabel.text = "k = \(k)"
            updateResult()
        }
    }
    var n:Int = 3 {
        didSet {
            nLabel.text = "n = \(n)"
            updateResult()
        }
    }
    
    //MARK: UIVievController
    
    override func viewWillAppear(_ animated: Bool) {
        k = 3
        kStepper.value = 3
        n = 5
        nStepper.value = 5
    }
    
    override func viewDidLoad() {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Actions
    
    @IBAction func stepperValueChanged(_ sender: Any) {
        if (kStepper.value > nStepper.value) {
            kStepper.value = nStepper.value
        }
        
        k = Int(kStepper.value)
        n = Int(nStepper.value)
    }
    
    //MARK: PrivateFunctions
    func newton(n:Int, k:Int) -> Int {
        if k > n {
            fatalError("Invalid data")
        }
        var top = [Int]((k+1)..<(n+1))
        var bottom = [Int](1..<(n-k+1))
        
        for (bid, b) in bottom.enumerated() {
            for (tid, t) in top.enumerated() {
                if t != 1 && b != 1 {
                    if t % bottom[bid] == 0 {
                        top[tid] = t / bottom[bid]
                        bottom[bid] = 1
                        continue
                    }
                }
            }
        }
        
        let topVal = top.reduce(1, {$1 * $0})
        let bottomVal = bottom.reduce(1, {$1 * $0})
        
        return topVal / bottomVal
    }
    
    func updateResult() {
        noOfCombinationsLabel.text = "Ilość kombinacji: \(newton(n: n, k: k))"
    }
    
}

